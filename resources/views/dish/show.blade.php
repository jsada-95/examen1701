@extends('layouts.app')
@section('content')

    <h1>Plato {{ $dish->id }}</h1>
    <p>Nombre: {{ $dish->name }} </p>
    <p>Descripcion: {{ $dish->description }} </p>

    <h2>Detalle</h2>
    <table class = "table">
    <tr>
        <th>Nombre</th>
        <th>Cantidad</th>
    </tr>
    @foreach ($dish->ingredients as $ingredient)
        <tr>
            <td>{{ $ingredient->name }} </td>
            <td>{{ $ingredient->pivot->quantity }}</td>
        </tr>
    @endforeach
    </table>
@endsection('content')