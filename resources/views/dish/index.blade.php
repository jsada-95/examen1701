@extends('layouts.app')

@section('content')
    <h1>Lista de platos</h1>
    
        <a href="/dishes/create">Nuevo</a>
    

    <table class="table">
        <thead>
            <tr>
                <th>Nombre de plato</th>
                <th>Descripción</th>
                <th>Nombre de usuario</th>
                <th>Tipo de plato</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($dishes as $dish)
            <tr>
                
                <td>  {{ $dish->name }} </td>
                <td>  {{ $dish->description }} </td>
                <td></td>
                <td></td>
                <td>  
                    <form method="post" action="/dishes/{{ $dish->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                      
                        <input type="submit" value="Borrar">            
                   
                        <a href="/dishes/{{ $dish->id }}"> Ver </a>
                       
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $dishes->render() }}

@endsection('content')