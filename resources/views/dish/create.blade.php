@extends('layouts.app')

@section('content')

<h1>Detalle de plato</h1>
<div class="form">
<form action="/dishes" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Descripcion: </label>
        <input type="text" name="description" value="{{ old('description') }}">
        {{ $errors->first('description') }}
    </div>

    <div class="form-group">
        <label>Tipo: </label>
        <select type="select" name="type_id" value="{{ old('type_id') }}">
            @foreach ($types as $type)
            <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('type_id') }}
    </div>

    <div class="form-group">
        <label>Usuario: </label>
        <input type="text" name="user_id" value="{{ old('user_id', $dishes->user_id) }} " readonly>
        {{ $errors->first('description') }}
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>
</form>
</div>

@endsection('content')