<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = ['name', 'description', 'type_id', 'user_id'];
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient')->withPivot('quantity');
    }
    public function types()
    {
        return $this->belongsTo('App\Type');
    }

}
