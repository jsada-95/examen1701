<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DishPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Family $family)
    {
        return true;
        // return $user->isAdmin();
    }

    public function create(User $user)
    {
        return true;
    }

    public function delete(User $user, Dish $dish)
    {
        return $user->isAdmin();
    }
}
